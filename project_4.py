import csv
import matplotlib.pyplot as plt
matches_list = []

# Populate matches list by reading matches.csv 
with open("matches.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file)
    for line in csv_reader:
        matches_list.append(line)

matches_list = matches_list[1:]
deliveries_list = []

with open("deliveries.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file)
    for line in csv_reader:
        deliveries_list.append(line)

deliveries_list = deliveries_list[1:]
year_id = set()

for match in matches_list:
    if int(match[1]) == 2015:
        year_id.add(match[0])

bowler_set = set()
bowler_dict = {}
id_col = 0
bowler_col = 8
runs_col = 17

for delivery in deliveries_list:
    if delivery[id_col] in year_id:
        if delivery[bowler_col] not in bowler_set:
            bowler_dict[delivery[bowler_col]] = [int(delivery[runs_col])]
            bowler_set.add(delivery[bowler_col])
        else:
            bowler_dict[delivery[bowler_col]].append(int(delivery[runs_col]))


def list_mean(lis):
    total = 0
    count = 0
    for i in lis:
        total+=i
        count +=1
    return total/count


for key in bowler_dict.keys():
    bowler_dict[key] = list_mean(bowler_dict[key])*6

eco_bowlers = [(k, bowler_dict[k]) for k in sorted(bowler_dict, key=bowler_dict.get, reverse=False)]
bowler_name = []
bowler_economy = []
bowler_index = 0
economy_index = 1

for bowler in eco_bowlers:
    bowler_name.append(bowler[bowler_index])
    bowler_economy.append(bowler[economy_index])
    
plt.figure(figsize=(20,10))
plt.bar(bowler_name[:10], bowler_economy [ :10])
plt.title("Top economic Bowlers in 2015")
plt.xlabel("Bowler")
plt.ylabel("Economy")
plt.show()