import csv
import matplotlib.pyplot as plt
matches_by_year = {}
with open("matches.csv", "r") as csv_file:
    matches_reader = csv.DictReader(csv_file)
    for match in matches_reader:
        if int(match["season"]) not in matches_by_year.keys():
            matches_by_year[int(match["season"])] = 1
        else:
            matches_by_year[int(match["season"])] += 1
plt.bar(matches_by_year.keys(), matches_by_year.values(), label='Bar')
plt.title("Matches Per Year")
plt.xlabel("Year")
plt.ylabel("Number of Matches")
plt.show()