import csv
import matplotlib.pyplot as plt
matches_by_year = {}
with open("matches.csv", "r") as csv_file:
    matches_reader = csv.DictReader(csv_file)
    for match in matches_reader:
        if int(match["season"]) not in matches_by_year.keys():
            matches_by_year[int(match["season"])] = [int(match["id"])]
        else:
            matches_by_year[int(match["season"])].append(int(match["id"]))

year = 2016
team_set = set()
extra_runs_per_team = {}
with open("deliveries.csv", "r") as csv_file:
    deliveries_reader = csv.DictReader(csv_file)
    for delivery in deliveries_reader:
        if int(delivery["match_id"]) in matches_by_year[year]:
            if delivery["bowling_team"] not in team_set:
                extra_runs_per_team[delivery["bowling_team"]] = int(delivery["extra_runs"])
                team_set.add(delivery["bowling_team"])
            else:
                extra_runs_per_team[delivery["bowling_team"]] += int(delivery["extra_runs"])

plt.figure(figsize=(10,5))
plt.bar(extra_runs_per_team.keys(), extra_runs_per_team.values(), label='Bar')
plt.xticks(rotation=15)
plt.title("Extra runs per team")
plt.xlabel("Year")
plt.ylabel("Extras")
plt.show()