import csv
import matplotlib.pyplot as plt
matches_list = []
with open("matches.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file)
    for match in csv_reader:
        matches_list.append(match)
matches_list = matches_list[1:]
team_list = list()
teams_dict = dict()
year = 2008
year_list = []
for _ in range(10):
    year_list.append(year)
    year+=1
team_col = 10
season_col = 1
for match in matches_list:
    if match[team_col] not in team_list:
        team_list.append(match[team_col])
        teams_dict[match[team_col]] = dict()
        for year in year_list:
            teams_dict[match[team_col]][year] = 0
        teams_dict[match[team_col]][int(match[season_col])] = 1
        
    else:
        if int(match[season_col]) not in teams_dict[match[team_col]].keys():
            teams_dict[match[team_col]][int(match[season_col])] = 1
        else:
            teams_dict[match[team_col]][int(match[season_col])] += 1
del team_list[-2]
len(team_list)
del teams_dict['']
color_dict = {
    'Mumbai Indians': 'blue',
    'Royal Challengers Bangalore': 'green',
    'Sunrisers Hyderabad': 'orange',
    'Chennai Super Kings': 'yellow',
    'Deccan Chargers': 'indigo',
    'Gujarat Lions': 'magenta',
    'Kings XI Punjab': 'red',
    'Kochi Tuskers Kerala': 'violet',
    'Kolkata Knight Riders': 'purple',
    'Pune Warriors': 'cyan',
    'Rajasthan Royals': 'pink',
    'Rising Pune Supergiant': 'gray',
    'Rising Pune Supergiants': 'gray',
    'Delhi Daredevils': 'black'
    
}
team_total = None
for team_name in team_list:
    if team_total is None:
        team_total=teams_dict[team_name].values()
    else:
        team_total = [int(x) + int(y) for x, y in zip(teams_dict[team_name].values(), team_total)]


for team_name in team_list:
        plt.bar(year_list, team_total, color=color_dict[team_name], label=team_name)
        team_total = [int(y) - int(x) for x, y in zip(teams_dict[team_name].values(), team_total)]

plt.legend(fontsize="x-small")
plt.show()