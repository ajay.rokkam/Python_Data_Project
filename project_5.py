import csv
import matplotlib.pyplot as plt
matches_by_year = {}

with open("matches.csv", "r") as csv_file:
    matches_reader = csv.DictReader(csv_file)
    for match in matches_reader:
        matches_by_year[int(match["id"])] = int(match["season"])

runs_by_year = {}

boundary = 6
with open("deliveries.csv", "r") as deliveries_file:
    deliveries_reader = csv.DictReader(deliveries_file)
    for delivery in deliveries_reader:
        if matches_by_year[int(delivery["match_id"])] not in runs_by_year.keys() and int(delivery["batsman_runs"]) == boundary:
            runs_by_year[matches_by_year[int(delivery["match_id"])]] = 1
        elif int(delivery["batsman_runs"]) == boundary:
            runs_by_year[matches_by_year[int(delivery["match_id"])]] += 1

plt.figure(figsize=(20,10))
plt.bar(runs_by_year.keys(), runs_by_year.values())
plt.title("Total number of sixes by Year")
plt.xlabel("year")
plt.ylabel("Number of sixes")
plt.show()
